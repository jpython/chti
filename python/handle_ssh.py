#!/usr/bin/env python3

from paramiko.ssh_exception import SSHException,NoValidConnectionsError 
import socket,sys

def connect_to_host(client,host,username='root'):
    '''Connexion SSH avec gestion des erreurs raisonnables.'''
    try:
        client.connect(hostname=host,username=username)
    except SSHException:
        sys.stderr.write("Échec de l'authentification SSH pour {}.\n".format(host))
        exit(3)
    except socket.gaierror:
        sys.stderr.write("Impossible d'identifier {}.\n".format(host))
        exit(5)
    except NoValidConnectionsError:
        sys.stderr.write("Échec de la connexion {}.\n".format(host))
        exit(5)

